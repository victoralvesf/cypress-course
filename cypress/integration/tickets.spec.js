describe('Tickets', () => {
  beforeEach(() => {
    cy.visit('https://ticket-box.s3.eu-central-1.amazonaws.com/index.html')
  })

  it("header's text should be 'TICKETBOX'", () => {
    cy.get('header h1').should('have.text', 'TICKETBOX')
  });

  it('fill all input fields', () => {
    const firstName = 'Victor'
    const lastName = 'Fialho'

    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type('myemail@example.com');
    cy.get('#requests').type('Vegetarian');
    cy.get('#signature').type(`${firstName} ${lastName}`);
  });

  it('select two tickets', () => {
    cy.get('#ticket-quantity').select('2');
  });

  it('check vip ticket type', () => {
    cy.get('#vip').check();
  });

  it('selects social media checkbox', () => {
    cy.get('#social-media').check();
  });

  it('select "friend" and "publication" checkboxes, then uncheck "friend"', () => {
    cy.get('#friend').check();
    cy.get('#publication').check();
    cy.get('#friend').uncheck();
  });

  it('alerts on invalid email', () => {
    cy.get('#email')
      .type('myemail-gmail.com')
      .should('have.class', 'invalid')
      .clear()
      .type('myemail@gmail.com')
      .should('not.have.class', 'invalid')
  });

  it('fill and submit the form', () => {
    const firstName = 'John'
    const lastName = 'Doe'
    const fullName = `${firstName} ${lastName}`
    const email = (firstName + lastName).toLowerCase() + '@mailbox.com'

    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type(email);

    cy.get('#ticket-quantity').select('2');
    cy.get('#vip').check();
    cy.get('#social-media').check();

    cy.get('#requests').type('Sugarless deserts.');

    cy.get('.agreement p').should('contain', `I, ${fullName}, wish to buy 2 VIP tickets.`);
    cy.get('#agree').check();

    cy.get('#signature').type(fullName);

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled');

    cy.get('@submitButton').click();

    cy.get('.success').should('have.text', 'Ticket(s) successfully ordered.');

    cy.get('@submitButton').should('be.disabled');
  });

  it('fill required fields and uncheck agreement', () => {
    const customer = {
      firstName: 'Victor',
      lastName: 'Fialho',
      email: 'victorfialho@example.com'
    }

    cy.fillRequiredFields(customer);

    cy.get('button[type="submit"]')
      .as('submitButton')
      .should('not.be.disabled');

    cy.get('#agree').uncheck();

    cy.get('@submitButton').should('be.disabled');
  });
});